import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import ItemCard from './ItemCard'
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
    root: {
      flexGrow: 1,
      margin: 'auto',
      maxWidth: '100%'
      //margin: '10px 2% auto 2%'
  
    },
  
    control: {
      padding: theme.spacing.unit * 2,
    },
  });

  
class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user, users, classes } = this.props;
        const {searchResult} = users;
        let result = ' ';
        if(searchResult){
            result = searchResult.map(itemValue => (
                <Grid key={itemValue.ItemID} item>
                    <ItemCard item={itemValue} />
                </Grid>));
        }
        let showRes = (
            <Grid container className={classes.root} spacing={8} justify='center' >
                {result}
            </Grid>);

        if (result.length == 0)
            showRes = <Typography variant='h4'>Sorry, cannot found.</Typography>;

        return (
            <div>
                {(user) && (
                    <div className="col-md-6 col-md-offset-3">
                        <h2>Hi userID: {user.userID}!</h2>
                        <p>You're logged in !!</p>
                        {users.loading && <em>Loading users...</em>}
                        {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                        <p>
                            <Link to="/login">Logout</Link>
                        </p>
                    </div>
                )}
                {showRes}
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(withStyles(styles)(HomePage));
export { connectedHomePage as HomePage };